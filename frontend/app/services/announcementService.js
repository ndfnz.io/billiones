(function() {
	'use strict';

	angular.module("app").factory("AnnouncementService", function(Restangular) {
		return {
			getAnnouncements: function() {
				return Restangular.all("announcements").getList().$object;
			},
			getAnnouncement: function(id) {
				return Restangular.one("announcement", id).get().$object;
			}
		};
	});

}());
