(function() {
	'use strict';

	angular.module("app").factory("PersonService", function(Restangular) {
		return {
			getPersons: function() {
				return Restangular.all("persons").getList().$object;
			},
			getPerson: function(id) {
				return Restangular.one("person", id).get().$object;
			},
			getPositionPersons: function(position) {
				return Restangular.all("persons/"+position).getList().$object;
			},
			searchPerson: function(query) {
				return Restangular.all("search/" + query).getList();
			},
			getExpensesSummary: function(id) {
				return Restangular.all("person/" + id + "/expenses/summary").getList().$object;
			},
			getDonationsSummary: function(id) {
				return Restangular.all("person/" + id + "/donations/summary").getList().$object;
			},
			getSalns: function(id) {
				return Restangular.all("person/" + id + "/salns").getList().$object;
			}
		};
	});

}());
