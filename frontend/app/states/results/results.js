(function() {
	"use strict";

	angular.module("app.states.results", [
		"restangular",
		"ui.router"
	])
	.config(function($stateProvider) {
		$stateProvider.state("results", {
			url: "/results",
			templateUrl: "app/states/results/results.html",
			controller: "ResultsController",
			resolve: {
				$title: function() { return "Results"; }
			}
		});
	}).controller("ResultsController", function($rootScope, $scope, $timeout, PersonService, GameService) {
		$scope.water = (Math.round(parseFloat($rootScope.allocation.water)*100)/100) + "%";
		$scope.education = (Math.round(parseFloat($rootScope.allocation.education)*100)/100) + "%";
		$scope.transportation = (Math.round(parseFloat($rootScope.allocation.transportation)*100)/100) + "%";
		$scope.infrastructure = (Math.round(parseFloat($rootScope.allocation.infrastructure)*100)/100) + "%";
		$scope.livelihood = (Math.round(parseFloat($rootScope.allocation.livelihood)*100)/100) + "%";
		$scope.correlation_person = {};

		GameService.computeCorrelation($rootScope.allocation);
		$timeout(function() {
			$scope.correlation_person_1_first_name = $rootScope.correlation_person_first[0].person_firstName;
			$scope.correlation_person_1_last_name = $rootScope.correlation_person_first[0].person_lastName;
			$scope.correlation_person_1_position = $rootScope.correlation_person_first[0].person_position;
			$scope.correlation_person_1_id = $rootScope.correlation_person_first[0].person_id;
			$scope.correlation_person_2_first_name = $rootScope.correlation_person_second[0].person_firstName;
			$scope.correlation_person_2_last_name = $rootScope.correlation_person_second[0].person_lastName;
			$scope.correlation_person_2_position = $rootScope.correlation_person_second[0].person_position;
			$scope.correlation_person_2_id = $rootScope.correlation_person_second[0].person_id;
			$scope.correlation_person_3_first_name = $rootScope.correlation_person_third[0].person_firstName;
			$scope.correlation_person_3_last_name = $rootScope.correlation_person_third[0].person_lastName;
			$scope.correlation_person_3_position = $rootScope.correlation_person_third[0].person_position;
			$scope.correlation_person_3_id = $rootScope.correlation_person_third[0].person_id;
		}, 750);
	});
}());