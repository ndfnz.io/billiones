(function() {
	"use strict";

	angular.module("app.states.home", [
		"restangular",
		"ui.router"
	])
	.config(function($stateProvider) {
		$stateProvider.state("home", {
			url: "/",
			templateUrl: "app/states/home/home.html",
			controller: "HomeController",
			resolve: {
				$title: function() { return "Home"; }
			}
		});
	}).controller("HomeController", function($scope, $timeout, PersonService, AnnouncementService) {
		$('.ui.massive.icon.button')
 			 .popup();
		$(".ui.accordion").accordion();
		$scope.announcements = AnnouncementService.getAnnouncements();
		$scope.emptyQuery = true;
		$scope.resultsFound = false;
		$scope.results = [];
		$scope.queryTimer;

		$scope.president = PersonService.getPositionPersons("President");
		$scope.vicepresident = PersonService.getPositionPersons("Vice President");
		$scope.senators = PersonService.getPositionPersons("Senator");
		$scope.governors = PersonService.getPositionPersons("Governor");

		$scope.update = function() {
			if ($scope.query == "") {
				$scope.resultsFound = false;
				$scope.emptyQuery = true;
				$('.home-state .query-results').removeClass("loading");
			} else {
				$scope.emptyQuery = false;
				if ($scope.queryTimer) {
					$timeout.cancel($scope.queryTimer);
				}
				$('.home-state .query-results').addClass("loading");
				$scope.queryTimer = $timeout(function() {
					PersonService.searchPerson($scope.query).then(function(data) {
						if(data.length == 0) {
							$scope.resultsFound = false;
							$scope.results = [];
						} else {
							$scope.resultsFound = true;
							$scope.results = data;
						}
						$('.home-state .query-results').removeClass("loading");
					});
				}, 500);
			}
		};
	});
}());