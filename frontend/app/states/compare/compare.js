(function() {
	"use strict";

	angular.module("app.states.compare", [
		"restangular",
		"ui.router"
	])
	.config(function($stateProvider) {
		$stateProvider.state("compare", {
			url: "/{person1Id:int}/compare/{person2Id:int}",
			templateUrl: "app/states/compare/compare.html",
			controller: "CompareController",
			resolve: {
				$title: function() { return "Compare"; }
			}
		});
	}).controller("CompareController", function($stateParams, $scope, PersonService) {
		$scope.person1 = PersonService.getPerson($stateParams.person1Id);
		$scope.expenses1 = PersonService.getExpensesSummary($stateParams.person1Id);
		$scope.donations1 = PersonService.getDonationsSummary($stateParams.person1Id);
		$scope.salns1 = PersonService.getSalns($stateParams.person1Id);
		$scope.person2 = PersonService.getPerson($stateParams.person2Id);
		$scope.expenses2 = PersonService.getExpensesSummary($stateParams.person2Id);
		$scope.donations2 = PersonService.getDonationsSummary($stateParams.person2Id);
		$scope.salns2 = PersonService.getSalns($stateParams.person2Id);
	});
}());