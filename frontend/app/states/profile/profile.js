(function() {
	"use strict";

	angular.module("app.states.profile", [
		"restangular",
		"ui.router"
	])
	.config(function($stateProvider) {
		$stateProvider.state("profile", {
			url: "/profiles/{personId:int}",
			templateUrl: "app/states/profile/profile.html",
			controller: "ProfileController",
			resolve: {
				$title: function() { return "Profile"; }
			}
		});
	}).controller("ProfileController", function($scope, $timeout, $stateParams, $sce, PersonService) {
		$scope.emptyQuery = true;
		$scope.resultsFound = false;
		$scope.results = [];
		$scope.queryTimer;

		$scope.person = PersonService.getPerson($stateParams.personId);
		$scope.expenses = PersonService.getExpensesSummary($stateParams.personId);
		$scope.donations = PersonService.getDonationsSummary($stateParams.personId);
		$scope.salns = PersonService.getSalns($stateParams.personId);
		$scope.to_trusted = function(html_code) {
			return $sce.trustAsHtml(html_code);
		}

		$scope.update = function() {
			if ($scope.query == "") {
				$scope.resultsFound = false;
				$scope.emptyQuery = true;
				$('.profile-state .query-results').removeClass("loading");
			} else {
				$scope.emptyQuery = false;
				if ($scope.queryTimer) {
					$timeout.cancel($scope.queryTimer);
				}
				$('.profile-state .query-results').addClass("loading");
				$scope.queryTimer = $timeout(function() {
					PersonService.searchPerson($scope.query).then(function(data) {
						if(data.length == 0) {
							$scope.resultsFound = false;
							$scope.results = [];
						} else {
							$scope.resultsFound = true;
							$scope.results = data;
						}
						$('.profile-state .query-results').removeClass("loading");
					});
				}, 500);
			}
		};
	});
}());