(function() {
	// define a new directive for the global navigation interface
	angular.module("app.directives.expensesGraph", []).directive("expensesGraph", function() {
		return {
			restrict: "E",
			templateUrl: "app/directives/expensesGraph/expensesGraph.html",
			controller: 'ExpensesGraphCtrl'
		}
	}).controller("ExpensesGraphCtrl",function(){
		var margin = {top: 20, right: 50, bottom: 30, left: 50},
		    width = 960 - margin.left - margin.right,
		    height = 500 - margin.top - margin.bottom;

		var parseDate = d3.time.format("%d-%b-%y").parse,
		    bisectDate = d3.bisector(function(d) { return d.date; }).left,
		    formatValue = d3.format(",.2f"),
		    formatCurrency = function(d) { return formatValue(d) + "php"; };

		var x = d3.time.scale()
		    .range([0, width]);

		var y = d3.scale.linear()
		    .range([height, 0]);

		var xAxis = d3.svg.axis()
		    .scale(x)
		    .orient("bottom");

		var yAxis = d3.svg.axis()
		    .scale(y)
		    .orient("left");

		var line = d3.svg.line()
		    .x(function(d) { return x(d.date); })
		    .y(function(d) { return y(d.close); })
			.interpolate('basis');

		var area = d3.svg.area()
			.x(function(d) {return x(d.date);})
			.y0(height)
			.y1(function(d) {return y(d.close);})
			.interpolate('basis');

		var svg = d3.select("expenses-graph").append("svg")
		    .attr("width", width + margin.left + margin.right)
		    .attr("height", height + margin.top + margin.bottom)
		  .append("g")
		    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    	var data;
		d3.json("data.json", function(error, data) {
		  if (error) throw error;

		  data.forEach(function(d) {
		    d.date = parseDate(d.date);
		    d.close = +d.close;
		  });

		  data.sort(function(a, b) {
		    return a.date - b.date;
		  });

		  x.domain([data[0].date, data[data.length - 1].date]);
		  y.domain(d3.extent(data, function(d) { return d.close; }));

		 svg.append("path")
         	.attr("class", "area")
            .style('fill', 'steelblue')
            .attr("d", area(data));

		  svg.append("path")
		      .datum(data)
		      .attr("class", "line")
		      .attr("d", line);

		  var focus = svg.append("g")
		      .attr("class", "focus")
		      .style("display", "none");

		  focus.append("circle")
		      .attr("r", 4.5);

		  focus.append("text")
		      .attr("x", 9)
		      .attr("dy", ".35em");

		  svg.append("rect")
		      .attr("class", "overlay")
		      .attr("width", width)
		      .attr("height", height)
		      .on("mouseover", function() { focus.style("display", null); })
		      .on("mouseout", function() { focus.style("display", "none"); })
		      .on("mousemove", mousemove);

		  function mousemove() {
		    var x0 = x.invert(d3.mouse(this)[0]),
		        i = bisectDate(data, x0, 1),
		        d0 = data[i - 1],
		        d1 = data[i],
		        d = x0 - d0.date > d1.date - x0 ? d1 : d0;
		    focus.attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
		    focus.select("text").text(formatCurrency(d.close));
		  }
		});
	});
})();