<?php
	$app->get("/person/:id/donations/summary", function($id) use ($app, $db) {
		// query database
		$queryResult = $db->donation()->where("person_id", $id);

		// prepare array output
		$donations = array(
			2010 => 0,
			2011 => 0,
			2012 => 0,
			2013 => 0,
			2014 => 0,
			2015 => 0
		);
		foreach ($queryResult as $donation) {
			$donations[$donation["year"]] += $donation["amount"];
		}
		// format and send output
		ResponseHelper::echoResponse(200, $donations);
	});
?>