<?php
	$app->get("/announcements", function() use ($app, $db) {
		// query database
		$queryResult = $db->announcement();

		// prepare array output
		$output = array();
		foreach ($queryResult as $announcement) {
			$output[] = array(
				"announcement_id"			=> $announcement["id"],
				"announcement_title"		=> $announcement["title"],
				"announcement_summary"		=> $announcement["summary"],
				"announcement_content"		=> $announcement["content"],
				"announcement_timestamp"	=> $announcement["timestamp"]
			);
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>