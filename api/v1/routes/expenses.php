<?php
	$app->get("/expenses", function() use ($app, $db) {
		// query database
		$queryResult = $db->expense();

		// prepare array output
		$output = array();
		foreach ($queryResult as $expense) {
			$output[] = array(
				"expense_id"			=> $expense["id"],
				"expense_person_id"		=> $expense["person_id"],
				"expense_amount"		=> $expense["amount"],
				"expense_year"			=> $expense["year"],
				"expense_location_id"	=> $expense["location_id"],
				"expense_description"	=> $expense["description"],	
			);
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>