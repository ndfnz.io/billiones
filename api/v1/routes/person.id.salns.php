<?php
	$app->get("/person/:id/salns", function($id) use ($app, $db) {
		// query database
		$queryResult = $db->saln()->where("person_id", $id);

		// prepare array output
		$output = array();
		foreach ($queryResult as $saln) {
			$output[] = array(
				"saln_id"				=> $saln["id"],
				"saln_person_id"		=> $saln["person_id"],
				"saln_real_assets"		=> $saln["real_assets"],
				"saln_personal_assets"	=> $saln["personal_assets"],
				"saln_liabilities"		=> $saln["liabilities"],
				"saln_net_worth"		=> $saln["net_worth"],
				"saln_year"				=> $saln["year"]
			);
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>