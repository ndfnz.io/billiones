<?php
	$app->get("/init_person_allocations", function() use ($app, $db) {
		// get number of Persons
		$num = $db->person()->count();
		$output = array();
		$db->person_allocation()->delete();
		for($ctr = 1; $ctr <= $num; $ctr++) {
			$total = 0;
			$allocations = array(
				"water"=> 0,
				"education"=> 0,
				"transportation"=> 0,
				"infrastructure"=> 0,
				"livelihood"=> 0
			);
			$queryResult1 = $db->expense()->select("amount, category")->where("person_id", $ctr);
			foreach($queryResult1 as $expense) {
				$queryResult2 = $db->expense_minor_category()->select("major_category")->where("id", $expense["category"]);
				foreach($queryResult2 as $category) {
					$total += $expense["amount"];
					switch ($category["major_category"]) {
						case '1':
							$allocations["water"] += $expense["amount"];
							break;
						case '2':
							$allocations["education"] += $expense["amount"];
							break;
						case '3':
							$allocations["transportation"] += $expense["amount"];
							break;
						case '4':
							$allocations["infrastructure"] += $expense["amount"];
							break;
						case '5':
							$allocations["livelihood"] += $expense["amount"];
							break;
					}
				}
			}
			$allocations["water"] = round($allocations["water"] / $total * 100, 2);
			$allocations["education"] = round($allocations["education"] / $total * 100, 2);
			$allocations["transportation"] = round($allocations["transportation"] / $total * 100, 2);
			$allocations["infrastructure"] = round($allocations["infrastructure"] / $total * 100, 2);
			$allocations["livelihood"] = round($allocations["livelihood"] / $total * 100, 2);
			$data = array(
				"id" => $ctr,
				"person_id" => $ctr,
				"water" => $allocations["water"],
				"education" => $allocations["education"],
				"transportation" => $allocations["transportation"],
				"infrastructure" => $allocations["infrastructure"],
				"livelihood" => $allocations["livelihood"]
			);
			
			$db->person_allocation()->insert($data);
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>