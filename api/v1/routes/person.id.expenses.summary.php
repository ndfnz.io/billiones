<?php
	$app->get("/person/:id/expenses/summary", function($id) use ($app, $db) {
		// query database
		$queryResult = $db->expense()->where("person_id", $id);

		// prepare array output
		$expenses = array(
			2010 => 0,
			2011 => 0,
			2012 => 0,
			2013 => 0,
			2014 => 0,
			2015 => 0
		);
		foreach ($queryResult as $expense) {
			$expenses[$expense["year"]] += $expense["amount"];
		}
		// format and send output
		ResponseHelper::echoResponse(200, $expenses);
	});
?>