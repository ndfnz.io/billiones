<?php
	$app->get("/donations", function() use ($app, $db) {
		// query database
		$queryResult = $db->donation();

		// prepare array output
		$output = array();
		foreach ($queryResult as $donation) {
			$output[] = array(
				"donation_id"			=> $donation["id"],
				"donation_person_id"	=> $donation["person_id"],
				"donation_donor"		=> $donation["donor"],
				"donation_amount"		=> $donation["amount"],
				"donation_year"			=> $donation["year"],
			);
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>